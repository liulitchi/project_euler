/*
 * Project Euler problem 34: Digit factorials
 *
 * 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
 * Find the sum of all numbers which are equal to 
 * the sum of the factorial of their digits.
 * Note: As 1! = 1 and 2! = 2 are not sums they are not included.
 */
#include <stdio.h>
 
int factorial(int n)
{
	int fac = 1;
	for ( ; n > 1; n--)
		fac *= n;
	return fac;
}
 
int main(void)
{
	int count = 0;
	for (int i = 145; i < 1000000; i++)
	{
		int sum = 0;
		for (int x = i; x > 0; x /= 10)
			sum += factorial(x % 10);
		if (sum == i)
			count += i;
	}
	printf("%d\n", count);
 
	return 0;
}

