/*
 * Project Euler problem 31: Coin sums
 *
 * In the United Kingdom the currency is made up of pound (£) and pence (p). 
 * There are eight coins in general circulation:
 * 1p, 2p, 5p, 10p, 20p, 50p, £1 (100p), and £2 (200p).
 * It is possible to make £2 in the following way:
 * 1 × £1 + 1 × 50p + 2 × 20p + 1 × 5p + 1 × 2p + 3 × 1p
 * How many different ways can £2 be made using any number of coins?
 * 
 * Brute force under c languag, not a smart solution.
 */
#include <stdio.h>

int main(void) {
	int a, b, c, d, e, f, g;
	int count = 0;
	int sum = 200;
	for (a = 0; a <= 200; a++) {
		for (b = 0; b <= 100; b++) {
			for (c = 0; c <= 40; c++) {
				for (d = 0; d <= 20; d++) {
					for (e = 0; e <= 10; e++) {
						for (f = 0; f <= 4; f++) {
							for (g = 0; g <= 1; g++) {
								int collect = a + 2 * b + 5 * c + 
											10 * d + 20 * e + 
											50 * f + 100 * g;
								if (collect == sum)
								count ++;
							}
						}
					}
				}
			}
		}
	}
	printf("There's %d ways can £2 be made using any number of coins.\n", count + 2);
	return 0;
}
