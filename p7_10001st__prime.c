/*
 * Project Euler Problem 7: 10001st prime
 * By listing the first six prime numbers: 
 * 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * What is the 10 001st prime number?
 *
 * Mathematics Tip:
 * The nth prime is greater than n * ln(n) + n * ln(ln(n)) - n, 
 * and less than n * ln(n) + n * ln(ln(n))
 * we can check our solution, it is less or equal to int(n * ln(n * ln(n)))
 * when n = 10001, we get 104318 <= prime_num <= 114319
 */
#include <stdio.h>

int is_prime(int x); 

int main(void)
{
	// first prime is 2, we count 1, need to find the 10001st prime
    int count = 1;
	int NUM = 10001;
	int i = 3; // we will check numbers from 3
    for(; ; i += 2)  
    {
        if(is_prime(i)) 
		{
        	count++;
			if (count == NUM)
                break;
        }
	}
	printf("Prime count %d is %d\n", NUM, i); 
    return 0;
}

int is_prime(int x) 
{
	for (int j = 3; j * j <= x; j += 2) 
	{
		if (x % j == 0)
			return 0;
	}
	return 1;
}
