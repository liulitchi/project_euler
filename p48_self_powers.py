'''
Project Euler project 48: Self powers

The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.
Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
'''
plus = 0
# maybe we don't need this modulus opreation
mod = 10000000000  
for i in range(1, 1001):
	plus += (i ** i) % mod
x = str(plus)
y = x[-10:]
print(y)
