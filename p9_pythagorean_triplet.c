/*
 * Project Euler problem 9: Special Pythagorean triplet
 *
 * A Pythagorean triplet is a set of three natural numbers,
 * a < b < c, for which, a^2 + b^2 = c^2
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 */
#include <stdio.h>

int main(void) {
	for (int a = 1; a < 333; a++) {
		for (int b = a + 1; b < 500; b++) {
			int m = 1000 * (a + b);
			int n = 500000 + a * b;
			if (m == n)
				printf("a = %d, b = %d, c = %d, abc = %d\n", 
						a, b, 1000 - a - b, a * b * (1000 - a - b));
		}
	}
	return 0;
}
