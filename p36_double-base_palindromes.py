# check a 2-base number is palindrome?
def bin_palind(x):
	y = str(bin(x))[2:]   # change 0b1011 to '1011'
	if y[::-1] == y:
		return True

# if in 1 digit, we know there's only 5 palindome number: 
# 1(0b1), 3(0b11), 5(0b101), 7(0b111), 9(0b1001)
# we got 1 + 3 + 5 + 7 + 9 = 25
total = 25

# if in 2 digits, we know num notion is 'aa', 
# which A must be an odd number 
for i in range(1, 10, 2):
	x2 = 11 * i
	if bin_palind(x2):
		total += x2

# if in 3 digits, we know num notion is 'aba', 
# which A must be an odd number, but b can be any number 
for i in range(1, 10, 2):
	for j in range(10):
		x3 = 101 * i + 10 * j
		if bin_palind(x3):
			total += x3

# if in 4 digits, we know num notion is 'abba', 
# which A must be an odd number, but b can be any number 
for i in range(1, 10, 2):
	for j in range(10):
		x4 = 1001 * i + 110 * j
		if bin_palind(x4):
			total += x4

# if in 5 digits, we know num notion is 'abcba',
# which A must be an odd number, but b & c can be any number
for i in range(1, 10, 2):
	for j in range(10):
		for k in range(10):
			x5 = 10001 * i + 1010 * j + 100 * k
			if bin_palind(x5):
				total += x5

# if in 6 digits, we know num notion is 'abccba',
# which A must be ani odd number, but b & c can be any number
for i in range(1, 10, 2):
    for j in range(10):
        for k in range(10):
            x6 = 100001 * i + 10010 * j + 1100 * k
            if bin_palind(x6):
                total += x6

print(total)

