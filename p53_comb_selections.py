'''
Project Euler problem 53: Combinatoric selections

There are exactly ten ways of selecting three from five, 12345:
123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
In combinatorics, C(5/3) = 10.
In general, C(n/r) = n! / [r!(n -r)!],  
where r <= n, n! = n * (n-1) * ... 3 * 2 * 1, and 0! = 1.
It is not until n = 23, that a value exceeds one-million:
How many, not necessarily distinct, values of C(n/r)
for 1 <= n <= 100, are greater than one-million?
'''
import math
sum = 0
for n in range(23, 101):
	for r in range(0, n):
		if math.comb(n, r) > 1000000:
			sum += 1
print(sum)
