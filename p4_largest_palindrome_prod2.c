/*
 * project euler problem 4: Largest palindrome product

 * A palindromic number reads the same both ways. 
 * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 ×  99.
 * Find the largest palindrome made from the product of two 3-digit numbers.

 * NOT a pretty solution ^_^
 */
#include <stdio.h>

int main(void)
{
	for (int i = 100; i < 1000; i++) {
		for (int j = i; j < 1000; j++) {
			if (i * j > 900000) {
				int m = i * j;
				if (((m / 100000) == m % 10) && 
					((m / 10000) % 10 == (m / 10) % 10) && 
					((m / 1000) % 10 == (m / 100) % 10))
					printf("%d = %d * %d\n", m, i, j);
			}
		}
	}
	return 0;
}

