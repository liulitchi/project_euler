/*
 * Euler project problem 14: Longest Collatz sequence
 *
 * The following iterative sequence is defined for the set of positive integers:
 *		n →  n/2 (n is even)
 *		n →  3n + 1 (n is odd)
 * Using the rule above and starting with 13, we generate the following sequence:
 *		13 →  40 →  20 →  10 →  5 →  16 →  8 →  4 →  2 →  1
 *
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. 
 * Although it has not been proved yet (Collatz Problem), 
 * it is thought that all starting numbers finish at 1.
 * Which starting number, under one million, produces the longest chain?
 *
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */

#include <stdio.h>

int main(void) {
	int max_chain = 0;      // value of the longest chain
	int fit_num;            // the number we need to find
	int i;
	unsigned long j;
	// every number n under 500,000 has corresponding 
	// reverse map 2n in upper half.
	// Odd number n, reverse maps to and even number in upper 
	// half which in turn maps to a number in lower half.
	for (i = 499999; i < 1000001; i += 2) {
		j = i;
		int term = 1;
		// also we can wrire while(j > 1)
		while (j != 1) {
			// 1 for true, 0 for false
			j = (j % 2) ? (3 * j + 1) : (j / 2);
			term++;
			if (term > max_chain) {
                max_chain = term;
                fit_num = i;
            }
		}
	}
	printf("%d, %d\n", max_chain, fit_num);
	return 0;
}
