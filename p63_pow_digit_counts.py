"""
Problem 63: Powerful digit counts
The 5-digit number, 16807 = 7^5, is also a fifth power. 
Similarly, the 9-digit number, 134217728 = 8^9, is a ninth power.

How many n-digit positive integers exist which are also an nth power?
"""

# 10 ^ (k - 1) < n ^ k < 10 ^ k
# we can tell that 1 <= n < 10, and 1 <= k < 22
# then we find (n ^ k)'s digits equsls to k

count = 0
for n in range(1, 10):
	for k in range(1, 22):
		if len(str(n ** k)) == k:
			count += 1
print(count)
