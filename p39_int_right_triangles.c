/* Project Euler Problem 39: Integer right triangles
 * 
 * If p is the perimeter of a right angle triangle 
 * with integral length sides, {a,b,c}, 
 * there are exactly three solutions for p = 120.
 * {20,48,52}, {24,45,51}, {30,40,50}
 * For which value of p ≤ 1000, is the number of solutions maximised?
 */
#include <stdio.h>

int main(void) 
{	
	int max_count = 0;
	int result = 0;
	// p <= 1000, and p is even
	for (int p = 12; p <= 1000; p += 2)
	{	
		int count = 0;
		// a <= b < c
		for (int a = 3; a < p / 3; a++) 
		{	
			// b = (p^2 - 2ap) / (2p - 2a) 
			if (p * (p - 2 * a) % (2 * (p - a)) == 0)
			{
				count++;
				if (count > max_count)
				{
					max_count = count;
					result = p;
				}
			}
		}
	}
	printf("The number of solutions maximised equals %d\n", result);
	return 0;
}
