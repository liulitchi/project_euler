/*
 * Project Euler problem 10: Summation of primes
 *
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 */
#include <stdio.h>
 
unsigned long long score = 2ULL;       // first prime
int NUM = 2000000;  // prime range

int is_prime(int x);

int main (void) {
	for (int i = 3; i < NUM; i += 2) {
		if (is_prime(i)) 
			score += i;
	}
	printf("Sum of all primes equals %llu\n", score);
	return 0;
}

int is_prime(int x) {
	for (int j = 3; j * j <= x; j += 2) {
		if (x % j == 0)
			return 0;
	}
	return 1;
}

