/*
 * Project Euler problem 3: Largest prime factor
 *
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
 *
 * reference: https://www.geeksforgeeks.org/c-program-for-find-largest-prime-factor-of-a-number
 */
#include <stdio.h>
#include <math.h>

long long max_prime_factor(long long x);

int main(void)
{
    long long n = 600851475143;
    printf("The largest prime factor is %lld.\n", max_prime_factor(n));

    return 0;
}

// A function to find largest prime factor 
long long max_prime_factor(long long x)
{
    // Initialize the maximum prime factor variable with the lowest one 
    long long max_prime = 2;

    // n must be odd at this point, so we can only iterate for odd integers 
    for (int i = 3; i <= sqrt(x); i += 2) {
        while (x % i == 0) {
            x /= i;
            max_prime = x;
        }
    }
    return max_prime;
}
